import numpy as np 
np.set_printoptions(threshold=np.inf)


f_analysis  = open("./output/analysis.dat", 'w')
f_hwhistory = open("./output/emhistory.dat", 'r')
#f_hwhistory = open("./output/hwhistory.dat", 'r')
f_emhistory = open("./output/emhistory.dat", 'r')
#f_swhistory = open("./output/403his.log", 'r')
f_swhistory = open("./input/403/403.gcc.200.history.log", 'r')
#f_history = open("./input/403/t.log", 'r')

hwhistory = []
swhistory = []

for line in f_hwhistory:
    hwhistory.append([float(x) for x in line.split()])

#print(hwhistory, file=f_analysis)

hwhistory = np.array(hwhistory)

for line in f_swhistory:
   swhistory.append([float(x) for x in line.split()])

#print(swhistory, file=f_analysis)

swhistory = np.array(swhistory)

row,cloum = swhistory.shape

shhisdleta = swhistory-hwhistory
armedelta = [0]*row  #Arithmetic Mean
romedelta = [0]*row  #Root Mean

for i in range(0, row):
	armetemp = 0
	rometemp = 0
	for j in range(0, cloum):
		armetemp  = armetemp+shhisdleta[i][j]
		rometemp  = rometemp+pow(shhisdleta[i][j],2)
	armedelta[i] = armetemp/1024
	romedelta[i] = np.sqrt(rometemp/1024)

for i in range(0, row):
	swhistory[i,0]=0
	hwhistory[i,0]=0
	swmax_history = max(swhistory[i])
	hwmax_history = max(hwhistory[i])
	if(swmax_history>1):
		swmax_hisaddr = np.where(swhistory[i]==swmax_history) 
		swmax_hisaddr = swmax_hisaddr[0][0]-1
	else:   swmax_hisaddr = 0
	if(hwmax_history>1):
		hwmax_hisaddr = np.where(hwhistory[i]==hwmax_history) 
		hwmax_hisaddr = hwmax_hisaddr[0][0]-1
	else:   hwmax_hisaddr = 0
	sw_dect = bool(swmax_history>=160)
	hw_dect = bool(hwmax_history>=160)
	if(sw_dect): sw_dectS = "SWDECT"
	else:        sw_dectS = "SWNDEC"
	if(hw_dect): hw_dectS = "HWDECT"
	else:        hw_dectS = "HWNDEC"
	if(sw_dect != hw_dect):
		swhw_match="error"
	else:
		swhw_match="right"		
	print("%8d"%(4096*(i+1)),"%-8.4f"%(swmax_history), "%4d"%(swmax_hisaddr), sw_dectS,\
                                 "%-8.4f"%(hwmax_history), "%4d"%(hwmax_hisaddr), hw_dectS,\
                                  swhw_match, "%-10.5f"%(armedelta[i]), "%-10.5f"%(romedelta[i]),\
                                                                                                  file=f_analysis)

f_hwhistory.close()
f_swhistory.close()
f_analysis.close()
