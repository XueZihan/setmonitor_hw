
//history_Ram
module history_Ram(
  input clk,
  input rst,
  input wen,
  input [9:0] waddr,
  input [12:0] wdata,
  input ren,
  input [9:0] raddr,
  output [12:0] rdata
);

reg [12:0] ram [1023:0]; //evicts 5bits + history 8bits
reg [9:0]  reg_raddr;

integer initvar;
initial begin
      #0.002;
      for (initvar = 0; initvar <= 1023; initvar = initvar+1)
        ram[initvar] = 12'd0;
end

// read and update ram evicts
  always @(posedge clk) begin
    if(ren) begin 
      reg_raddr <= raddr;
    end
    if(wen) begin
      ram[waddr] <= wdata;
    end
  end

  assign rdata = ram[reg_raddr];
  
endmodule

//max=31; 31^2=921
module Square(
  input clk,
  input rst,
  input valid,
  input[4:0]  int_fract, //integer fraction
  input[1:0]  dec_fract, //decimal fraction 11=>0.75 01=>0.25
  output[9:0] result_intf, //
  output[3:0] result_decf  //granularity = (1/4)^2=(1/16) 1001=>0.625
);

wire [4:0]  int_op1;
wire [3:0]  int_op2;
wire [5:0]  int_result;
reg  [9:0]  int_sq;

assign result_decf = {dec_fract[0] && int_fract[0], dec_fract[1], dec_fract[1] && dec_fract[0], dec_fract[0]};
assign result_intf = int_sq  + int_result;
assign int_result  = int_op1 + int_op2;
assign int_op1     = dec_fract[1] ? int_fract      : 5'd0;
assign int_op2     = dec_fract[0] ? int_fract[4:1] : 4'd0;
  
always @(int_fract) begin 
case (int_fract)
    5'd0:    int_sq=10'd0;
    5'd1:    int_sq=10'd1;
    5'd2:    int_sq=10'd4;
    5'd3:    int_sq=10'd9;
    5'd4:    int_sq=10'd16;
    5'd5:    int_sq=10'd25;
    5'd6:    int_sq=10'd36;
    5'd7:    int_sq=10'd49;
    5'd8:    int_sq=10'd64;
    5'd9:    int_sq=10'd81;
    5'd10:   int_sq=10'd100;
    5'd11:   int_sq=10'd121;
    5'd12:   int_sq=10'd144;
    5'd13:   int_sq=10'd169;
    5'd14:   int_sq=10'd196;
    5'd15:   int_sq=10'd225;
    5'd16:   int_sq=10'd256;
    5'd17:   int_sq=10'd289;
    5'd18:   int_sq=10'd324;
    5'd19:   int_sq=10'd361;
    5'd20:   int_sq=10'd400;
    5'd21:   int_sq=10'd441;
    5'd22:   int_sq=10'd484;
    5'd23:   int_sq=10'd529;
    5'd24:   int_sq=10'd576;
    5'd25:   int_sq=10'd625;
    5'd26:   int_sq=10'd676;
    5'd27:   int_sq=10'd729;
    5'd28:   int_sq=10'd784;
    5'd29:   int_sq=10'd841;
    5'd30:   int_sq=10'd900;
    default: int_sq=10'd961;
  endcase
end    

endmodule


//
module Accumlate(
  input clk,
  input rst,
  input         acc_en,
  input         acc_reset,   //Accumlate reset
  input[10:0]   squ_intf,    //Square integer fraction
  input[3:0]    squ_decf,     //Square decimal fraction
  output[17:0]  sac          //Square Accumulate
);

 reg  [17:0] sac_now;
 reg  [4:0]  sac_decnow;
 wire [17:0] sac_next;
 wire [4:0]  sac_decnext;
 //wire [18:0] sac_next_temp;
 //assign sac_next_temp = sac_now + squ;
 //assign sac_next      = sac_next_temp[18] ? 17'h1ffff : sac_next_temp[17:0];
 assign sac_next    = sac_now + squ_intf + sac_decnext[4];
 assign sac_decnext = sac_decnow[3:0] + squ_decf;
 assign sac         = acc_en ? sac_next
                    :          sac_now;

 always @(posedge clk) begin
    if(rst|| acc_reset) begin
      sac_now      <= 17'd0;
      sac_decnow   <= 5'd0;
    end else if(acc_en) begin
      sac_now    <= sac_next;
      sac_decnow <= sac_decnext;        

    end
 end

endmodule

// 1/qrm
module CaculateQrm(
  input clk,
  input rst,
  input[9:0]    squ,         //square 
  input         init,        //use squarea to predict the min 1/qrm
  input[17:0]   sac,         //square Accumulate
  input         sac_valid, 
  output        reachth,     //reach threshold
  //output        int_fract, //always 1
  output        dec_fract,
  output        shdir,       //0->left  1->right
  output[1:0]   shmnt
);

 reg  Reg_shdir;
 reg  [3:0]  qrmvalue;    //dec_fract + shdir + shmnt
 wire [3:0]  qrm_0_10,        qrm_10_20,       qrm_20_40,       
             qrm_40_84,       qrm_84_164,      qrm_164_334,     
             qrm_334_655,     qrm_655_1337,    qrm_1337_2621,   
             qrm_2621_5349,   qrm_5349_10485,  qrm_10485_21399, 
             qrm_21399_41493, qrm_41493_max;

 wire [18:0] qrm_0_10th,        qrm_10_20th,       qrm_20_40th,       
             qrm_40_84th,       qrm_84_164th,      qrm_164_334th,     
             qrm_334_655th,     qrm_655_1337th,    qrm_1337_2621th,   
             qrm_2621_5349th,   qrm_5349_10485th,  qrm_10485_21399th, 
             qrm_21399_41493th, qrm_41493_maxth;

//                         {dec_fract + shmnt}
assign qrm_0_10          = {1'd1,        3'd3}; //12
assign qrm_10_20         = {1'd0,        3'd3}; //8
assign qrm_20_40         = {1'd1,        3'd2}; //6
assign qrm_40_84         = {1'd0,        3'd2}; //4
assign qrm_84_164        = {1'd1,        3'd1}; //3
assign qrm_164_334       = {1'd0,        3'd1}; //2
assign qrm_334_655       = {1'd1,        3'd0}; //1.5
assign qrm_655_1337      = {1'd0,        3'd0}; //1
assign qrm_1337_2621     = {1'd1,        3'd1}; //0.75
assign qrm_2621_5349     = {1'd0,        3'd1}; //0.5
assign qrm_5349_10485    = {1'd1,        3'd2}; //0.375
assign qrm_10485_21399   = {1'd0,        3'd2}; //0.25
assign qrm_21399_41493   = {1'd1,        3'd3}; //0.1875
assign qrm_41493_max     = {1'd0,        3'd3}; //0.125

assign qrm_0_10th        = 18'd0;          //12
assign qrm_10_20th       = 18'd10;         //8
assign qrm_20_40th       = 18'd20;         //6
assign qrm_40_84th       = 18'd40;         //4
assign qrm_84_164th      = 18'd84;         //3
assign qrm_164_334th     = 18'd164;        //2
assign qrm_334_655th     = 18'd334;        //1.5
assign qrm_655_1337th    = 18'd655;        //1
assign qrm_1337_2621th   = 18'd1337;       //0.75
assign qrm_2621_5349th   = 18'd2621;       //0.5
assign qrm_5349_10485th  = 18'd5349;       //0.375
assign qrm_10485_21399th = 18'd10485;      //0.25
assign qrm_21399_41493th = 18'd21399;      //0.1875
assign qrm_41493_maxth   = 18'd41943;      //0.125

 logic [13:0]  sel;         //sel qrmvalue
 logic [13:0]  cmp;
 wire  [3:0]   qrmtemp_0,   qrmtemp_1,   qrmtemp_2, 
               qrmtemp_3,   qrmtemp_4,   qrmtemp_5,
               qrmtemp_6,   qrmtemp_7,   qrmtemp_8,
               qrmtemp_9,   qrmtemp_10,  qrmtemp_11,
               qrmtemp_12,  qrmtemp_13;

 assign cmp ={sac>qrm_41493_maxth,  sac>qrm_21399_41493th,  sac>qrm_10485_21399th, 
              sac>qrm_5349_10485th, sac>qrm_2621_5349th,    sac>qrm_1337_2621th,
              sac>qrm_655_1337th,   sac>qrm_334_655th,      sac>qrm_164_334th,
              sac>qrm_84_164th,     sac>qrm_40_84th,        sac>qrm_20_40th,
              sac>qrm_10_20th,      1'b1};
  
  assign sel = ~{1'b0,cmp[12:1]} & cmp;
  assign qrmtemp_0  = sel[0]  ? qrm_0_10        : 0;
  assign qrmtemp_1  = sel[1]  ? qrm_10_20       : 0;
  assign qrmtemp_2  = sel[2]  ? qrm_20_40       : 0;
  assign qrmtemp_3  = sel[3]  ? qrm_40_84       : 0;
  assign qrmtemp_4  = sel[4]  ? qrm_84_164      : 0;
  assign qrmtemp_5  = sel[5]  ? qrm_164_334     : 0;
  assign qrmtemp_6  = sel[6]  ? qrm_334_655     : 0;
  assign qrmtemp_7  = sel[7]  ? qrm_655_1337    : 0;
  assign qrmtemp_8  = sel[8]  ? qrm_1337_2621   : 0;
  assign qrmtemp_9  = sel[9]  ? qrm_2621_5349   : 0;
  assign qrmtemp_10 = sel[10] ? qrm_5349_10485  : 0;
  assign qrmtemp_11 = sel[11] ? qrm_10485_21399 : 0;
  assign qrmtemp_12 = sel[12] ? qrm_21399_41493 : 0;
  assign qrmtemp_13 = sel[13] ? qrm_41493_max   : 0;

always @(posedge clk) begin
  if(rst) begin
    Reg_shdir <= 1'b0;
    qrmvalue  <= 4'b0;
  end else begin
    Reg_shdir <= cmp[6];
    qrmvalue  <= qrmtemp_0  | qrmtemp_1    | qrmtemp_2   |
                 qrmtemp_3  | qrmtemp_4    | qrmtemp_5   |
                 qrmtemp_6  | qrmtemp_7    | qrmtemp_8   |
                 qrmtemp_9  | qrmtemp_10   | qrmtemp_11  |
                 qrmtemp_12 | qrmtemp_13;
  end
end


  assign dec_fract   = qrmvalue[3];
  assign shdir       = Reg_shdir;
  assign shmnt       = qrmvalue[2:0];

endmodule


module CaculateDelta(
  input clk,
  input rst,
  input[9:0]   squ,         //Square
  input        en,  
  //input        int_fract, //always 1
  input        dec_fract,
  input        shdir,       //0->left  1->right
  input[1:0]   shmnt,
  output[7:0]  delta
);

wire[12:0]    Lop1;
wire[11:0]    Lop2;
wire[13:0]    Lresult;
wire[9:0]     Rop1;
wire[8:0]     Rop2;
wire[10:0]    Rresult;
wire          full; //> 255
assign Lop1     = squ << shmnt;
assign Lop2     = dec_fract ? Lop1[12:1] : 9'd0;
assign Lresult  = Lop1+Lop2;
assign Rop1     = squ;
assign Rop2     = dec_fract ? squ[9:1] : 9'd0;
assign Rresult  = (Rop1 + Rop2) >> shmnt;
assign full     = shdir ? Rresult[10:8] != 3'd0
                :         Lresult[13:8] != 3'd0;
assign delta    = full  ? 8'd255
                : shdir ? Rresult[7:0]
                :         Lresult[7:0];

endmodule


module SetCSAMonitor(
  input  clk,
  input  rst,
  input  access,
  input  evicted,
  input[9:0]  set_addr,
  output      attack_detected,
  output[9:0] attack_addr,
  output      busy,
  input       test_stop
);

wire         ram_wen;
wire [9:0]   ram_waddr;
wire [12:0]  ram_wdata;
wire         ram_ren;
wire [9:0]   ram_raddr;
wire [12:0]  ram_rdata;
reg          s1_ren;     //ram_ren   delay 1 cyc
reg  [9:0]   s1_addr;    //ram_raddr delay 1 cyc
reg          s2_ren;     //ram_ren   delay 2 cyc
reg  [9:0]   s2_addr;    //ram_raddr delay 2 cyc
wire [4:0]   s1_evicts;            //read from ram
wire [5:0]   s1_new_evicts;        //update s1_new_evicts  = s1_evicts+1'd1;
reg  [5:0]   s2_evicts;            //update s2_evicts      <= s1_new_evicts;
wire [4:0]   s2_new_evicts;        //overflow ? write new_evicts back to ram
wire [7:0]   s1_history;           //read from ram
wire [7:0]   s1_new_history;       //flitter s1_new_history   <= s1_history*31/32
reg  [7:0]   s2_history;           //s2_history  <= s1_new_history;
reg  [8:0]   s2_new_history_temp;  //new_history_temp =s2_history+delta;
wire [7:0]   s2_new_history;       //overflow ? 0 < new_history < 160
wire         history_reachth;      //new_history_temp > 160

reg  [12:0]  total_access;
reg  [12:0]  total_evicts;

reg  [9:0]   monitor_rraddr;  

reg  [4:0]   mu;            //4 = 100.00; 3.75 = 011.11
reg  [6:0]   evi_mu;        //|evicts - mu|
wire         evi_mupos;     //positive?: evicts > mu
wire [4:0]   evi_muint;     //|evicts - mu| integer fraction
wire [1:0]   evi_mudec;     //|evicts - mu| decimal fraction
wire [9:0]   evi_musqu;          //|evicts - mu|^2 square
wire [3:0]   evi_musqu_decf;     //|evicts - mu|^2 square decimal fraction
reg  [9:0]   s2_evi_musqu;       //evi_musqu delay 1cyc
reg  [3:0]   s2_evi_musqu_decf;  //evi_musqu delay 1cyc

reg  [1:0] s0_state;       //0:idle, 1:qrm, 2:new_history
reg  [1:0] s0_state_next;  //0:idle, 1:qrm, 2:new_history
reg  [1:0] s1_state;       //0:idle, 1:qrm, 2:new_history
reg  [1:0] s2_state;       //0:idle, 1:qrm, 2:new_history
reg  [1:0] s3_state;

reg        deltapos;      //delta is positive

assign s0_state_next = (access           && total_access   == 12'd4095) ? 2'd1
                     : (s0_state == 2'd1 && monitor_rraddr == 10'd1023) ? 2'd2
                     : (s0_state == 2'd2 && monitor_rraddr == 10'd1023) ? 2'd0
                     :                                                    s0_state;

/*latch
//state machine
always @(*) begin
  if(access && total_access == 12'd4094) begin
    s0_state_next =  2'd1;
  end else if(monitor_rraddr == 12'd4095) begin
    if(s0_state == 2'd1) begin
      s0_state_next <= 2'd2;
    end else if(s0_state == 2'd2) begin
      s0_state_next <= 2'd0;
    end else begin
      s0_state_next <= s0_state;
    end 
  end else begin
    s0_state_next <= s0_state;
  end
end
*/

always @(posedge clk) begin
   if(rst) begin
     s0_state <= 2'd0;
     s1_state <= 2'd0;
     s2_state <= 2'd0;
     s3_state <= 2'd0;
   end else begin
     s0_state <= s0_state_next;
     s1_state <= s0_state;
     s2_state <= s1_state;
     s3_state <= s2_state;
   end
end


//stage 0
//read ram
//record evicts and access approach mu(average evicts) from max_mu
//generate monitor address
wire [12:0] total_access_next;
wire [12:0] total_evicts_next;
assign      total_access_next = total_access + access;
assign      total_evicts_next = total_evicts + evicted;

assign ram_ren   = s0_state == 2'd0 ? evicted  : 1'd1;
assign ram_raddr = s0_state == 2'd0 ? set_addr : monitor_rraddr;

always @(posedge clk) begin
   if(rst) begin
     s1_ren         <= 1'd0;
     s1_addr        <= 12'd0; 
     total_access   <= 12'd0;
     total_evicts   <= 12'd0;
     monitor_rraddr <= 10'd0; 
   end else begin
     s1_ren         <= ram_ren;
     s1_addr        <= ram_raddr;
     //monitor_rraddr is used for reading ram when caculate qac(mac) and delta
     if(s0_state != s0_state_next) begin
       monitor_rraddr <= 10'd0;
     end else if(s0_state != 2'd0) begin
       monitor_rraddr <= monitor_rraddr + 1'd1;
     end
     //access and evicts counter, approach mu(average evicts) from max_mu
     if(s0_state == 2'd2 && s0_state_next == 2'd0) begin
       total_access <= 12'd0;
       total_evicts <= 12'd0;
     end else if(s0_state == 2'd0) begin
       total_access <= total_access_next;
       total_evicts <= total_evicts_next;
       mu           <= total_evicts_next[12:8];
     end
   end
end


//ram
history_Ram history_Ram (
  .clk(clk),
  .rst(rst),
  .wen(ram_wen),
  .waddr(ram_waddr),
  .wdata(ram_wdata),
  .ren(ram_ren),
  .raddr(ram_raddr),
  .rdata(ram_rdata)
);

//stage 1
//in state qrm caculate square
//in state new_history caculate square
reg  [4:0]  max_evicts; //one set max evicts times;
assign  s1_history = ram_rdata[7:0];
assign  s1_evicts  = (ram_wen && (ram_waddr == s1_addr)) ? s2_new_evicts : ram_rdata[12:8];
assign  evi_mupos  = (s1_evicts[4] || s1_evicts[3] || s1_evicts[2:0]  > mu[4:2]);
assign  evi_muint  = evi_mu[6:2];
assign  evi_mudec  = evi_mu[1:0];       


always@(*) begin
  case (evi_mupos)
    1'b0:    evi_mu ={{mu[4:2] - s1_evicts[2:0]},  mu[1:0]};
    1'b1:    evi_mu ={s1_evicts ,  2'd0} - mu;
    default: evi_mu ={s1_evicts ,  2'd0} - mu;
  endcase  
end

//update evicts
assign s1_new_evicts  = (s1_state == 2'd0) ? s1_evicts + 1'd1
                      : (s1_state == 2'd2) ? 6'd0
                      :                      s1_evicts;
//flitter history
wire [2:0] history_minuend;
assign history_minuend = (s1_state == 2'd2 && s1_ren && s1_history[7:5] > 3'd0) ? s1_history[7:5]  //>31
                       : (s1_state == 2'd2 && s1_ren && s1_history[4]   > 3'd0) ? 3'd1             //>15
                       :                                                          3'd0;                             
assign s1_new_history = s1_history-history_minuend;              


always @(posedge clk) begin
   if(rst) begin 
     s2_ren       <= 1'd0;
     s2_addr      <= 12'd0;
     s2_evicts    <= 6'd0;
     s2_history   <= 7'd0;
     deltapos     <= 1'd0; 
     max_evicts   <= 5'd0;
     s2_evi_musqu <= 10'd0;
     s2_evi_musqu_decf <= 10'd0;
   end else begin
     s2_ren       <= s1_ren;
     s2_addr      <= s1_addr;
     deltapos     <= evi_mupos;
     s2_evi_musqu <= evi_musqu;
     s2_evi_musqu_decf <= evi_musqu_decf;
     //update evicts
     s2_evicts    <= s1_new_evicts;
     //flitter history
     s2_history   <= s1_new_history;
     //record the max evicts
     if(s1_state == 2'd0) begin
       if(s1_ren && (max_evicts < s1_new_evicts[4:0])) begin
         max_evicts <= s1_new_evicts[4:0];
       end
     end else begin
       max_evicts <= 5'd0;
     end
   end
end                  

Square evi_muSquare(
.clk(clk),
.rst(rst),
.valid(),
.int_fract(evi_muint),
.dec_fract(evi_mudec), //decimal fraction
.result_intf(evi_musqu),
.result_decf(evi_musqu_decf)
);

//stage2
//in qrm caculate 1/qrm
wire        qrm_dec;
wire        qrm_shdir;
wire [1:0]  qrm_shmnt;
wire [7:0]  delta_value;
wire        acc_en;     //enable signal for Accumlate module
wire        acc_rst;    //reset  signal for Accumlate module
wire [17:0] sac_value;  //square accumlate result value
wire        qrm_rst;    //qrm reset for CaculateQrm module
wire        sac_valid;  //square accumlate valid for CaculateQrm module
wire        qrm_init;   //square init for CaculateQrm
wire        cadelta_en; //enable caculate delta
reg         history_clr; //clear history
reg [9:0]   history_addr; //from this addr to clear this is attack addr
wire        reach_hisaddr; // 
wire        before_hisaddr; // 

assign acc_en           = s2_state == 2'd1;
assign acc_rst          = s2_state == 2'd0;
assign qrm_rst          = rst || ((s2_state == 2'd0) && (s3_state == 2'd2));
assign sac_valid        = s2_state == 2'd1;
assign qrm_init         = (s2_state == 1'd0) && s2_ren && deltapos;
assign cadelta_en       = s2_state == 2'd2;
//caculate new_evicts and new_history and write them back to ram
assign s2_new_evicts       = s2_evicts[5] ? 5'h1f: s2_evicts[4:0];
assign history_reachth     = deltapos && (s2_new_history_temp > 159);
assign s2_new_history      = (history_reachth || (!deltapos && s2_new_history_temp[8])) ? 7'h0 : s2_new_history_temp[7:0];
assign reach_hisaddr       = s2_addr == history_addr;
assign before_hisaddr      = s2_addr <  history_addr;
//ram write
assign ram_wdata        = {s2_new_evicts, s2_new_history};
assign ram_waddr        = s2_addr;
assign ram_wen          = s2_ren && (s2_state == 2'd0 || s2_state == 2'd2);   

always @(posedge clk) begin
  if(rst) begin
    history_clr  <= 1'b0;
    history_addr <= 10'd0;
  end else if(s2_state == 2'd2) begin
    if(history_reachth) begin
      history_clr  <= 1'b1;
      history_addr <= s2_addr;
    end else if(history_clr && reach_hisaddr) begin
      history_clr  <= 1'b0;
      history_addr <= 10'd0;
    end
  end
end

always @(*) begin
  s2_new_history_temp <= s2_history;
  if(s2_state == 2'd2) begin
    if(history_clr) begin
      s2_new_history_temp   <= 9'd0; 
      if(before_hisaddr && deltapos) begin
        s2_new_history_temp <= delta_value;
      end
    end else begin  //end if(history_clr)
      s2_new_history_temp <= s2_history - delta_value;
      if(deltapos) begin
        s2_new_history_temp <= s2_history + delta_value;        
      end
    end
  end
end


//square accumlate
Accumlate casac(
  .clk(clk),
  .rst(rst),
  .acc_en(acc_en),
  .acc_reset(acc_rst), 
  .squ_intf(s2_evi_musqu),      //square
  .squ_decf(s2_evi_musqu_decf),  //square decimal fraction
  .sac(sac_value)          //Square Accumulate
);

CaculateQrm caqrm(
  .clk(clk),
  .rst(qrm_rst),
  .squ(s2_evi_musqu),
  .init(qrm_init),
  .sac(sac_value),         //Square Accumulate
  .sac_valid(sac_valid),   //Square Accumulate valid
  .reachth(),
  //.int_fract(), //always 1
  .dec_fract(qrm_dec),
  .shdir(qrm_shdir),       //0->left  1->right
  .shmnt(qrm_shmnt)
);

CaculateDelta  cadelta(
  .clk(clk),
  .rst(rst),
  .squ(s2_evi_musqu),         //Square
  .en(cadelta_en),  
  .dec_fract(qrm_dec),
  .shdir(qrm_shdir),       //0->left  1->right
  .shmnt(qrm_shmnt),
  .delta(delta_value)
);

//output io
assign busy             = s1_state != 2'd0;
assign attack_addr      = s2_addr;
assign attack_detected  = history_reachth;

//some signal for debug
reg [12:0] evictsCR;     //evicts count
reg [12:0] max_evictsCR; //max evicts count
reg [4:0]  muR;
reg [4:0]  max_evictsR;     //one line max evicts
reg [4:0]  max_all_evictsR; //all input max evicts
reg [7:0]  max_deltaR;
reg [9:0]  max_deladdrR;
reg [8:0]  max_historyR;
reg [9:0]  max_hisaddrR;
reg [7:0]  max_hisdeltaR;
reg [17:0] sac_valueR;
reg [17:0] max_sac_valueR;
reg        attack_dectR;
reg [9:0]  attack_addrR;
reg        qrm_decR;
reg        qrm_shdirR;
reg [1:0]  qrm_shmntR;


always @(posedge clk) begin
  if(rst) begin
    muR              <= 4'b0;
    max_evictsR      <= 5'd0;
    max_all_evictsR  <= 5'd0;
    max_deltaR       <= 8'd0;
    max_deladdrR     <= 10'd0;
    max_historyR     <= 9'd0;
    max_hisdeltaR    <= 5'd0;
    max_hisaddrR     <= 10'd0;
    sac_valueR       <= 18'd0;
    max_sac_valueR   <= 18'd0;
    attack_dectR     <= 10'b0;
    attack_addrR     <= 10'd0; 
    qrm_decR         <= 1'b0;
    qrm_shdirR       <= 1'b0;
    qrm_shmntR       <= 2'b0;
    max_evictsCR     <= 12'd0;
  end else begin
    if(s2_state == 2'd0 && s3_state == 2'd2) begin
      max_evictsR   <= 5'd0;
      max_deltaR    <= 18'd0;
      max_deladdrR  <= 10'd0;
      max_historyR  <= 9'd0;
      max_hisdeltaR <= 5'd0;
      max_hisaddrR  <= 10'd0;
      attack_dectR  <= 10'd0;
      attack_addrR  <= 10'd0;
    end
    if(s0_state == 2'd2 && s0_state_next == 2'd0) begin
      muR <= mu;
      evictsCR <= total_evicts;
      if(max_evictsCR < total_evicts) begin
        max_evictsCR <= total_evicts;
      end
    end
    if(s1_state == 2'd1 && s0_state == 2'd2) begin
      sac_valueR <= sac_value;
      if(max_sac_valueR < sac_value) begin
        max_sac_valueR <= sac_value;
      end
    end
    if(s2_state == 2'd2 && (deltapos || !deltapos && !s2_new_history_temp[8]) && max_historyR < s2_new_history_temp) begin
      //deltapos means add
      //!deltapos && !s2_new_history_temp[8] means sub and not overflow(not<0)
      max_hisaddrR  <= s2_addr;
      max_historyR  <= s2_new_history_temp;
      max_hisdeltaR <= delta_value;
    end
    if(s2_state == 2'd2) begin
      qrm_decR   <= qrm_dec;
      qrm_shdirR <= qrm_shdir;
      qrm_shmntR <= qrm_shmnt;
    end
    if(s1_state == 2'd0 && s1_ren && max_evictsR < s1_new_evicts[4:0]) begin
      max_evictsR <= s1_new_evicts[4:0];
    end
    if(s1_state == 2'd0 && s1_ren && max_all_evictsR < s1_new_evicts[4:0]) begin
      max_all_evictsR <= s1_new_evicts[4:0];
    end
    if(s2_state == 2'd2 && delta_value > max_deltaR) begin
      max_deltaR    <= delta_value;
      max_deladdrR  <= s2_addr;
    end
    if(s2_state == 2'd2 && attack_detected) begin
      attack_dectR <= 1'd1;   
      attack_addrR <= attack_addr;     
    end
  end
end

//debug
longint unsigned initvar;
integer initvar_co;
integer param_file;
integer total_file;
integer evicts_file;
integer delta_file;
integer history_file;
integer evicts_infile; //input file
reg [63:0] evicts_orig;
reg [63:0] total_attack;

initial begin
  //evicts_infile = $fopen("./input/403.gcc.cp-decl.evict.log","r");
  evicts_infile = $fopen("./input/403/403.gcc.200.evict.log","r");
  param_file    = $fopen("./output/hwparameter.dat");
  total_file    = $fopen("./output/hwtotal.dat");
  //evicts_file   = $fopen("./output/hwevicts.dat");
  history_file  = $fopen("./output/hwhistory.dat");
  //delta_file    = $fopen("./output/hwdelta.dat");
end
always @(posedge clk) begin
 if(rst) begin
   initvar = 4096;
   initvar_co = 0;
   total_attack<= 64'd0;
 end else begin
    //write line number
    if(s0_state==2'd0 && s0_state_next==2'd1) begin
      initvar_co = initvar_co+1;
      if(initvar_co==1000) begin
        initvar_co = 0;
        $fwrite(32'h80000001,"%d attack= %d\n", initvar, total_attack);
      end
      if(initvar!=4096) begin
        $fwrite(param_file,   "\n");
        //$fwrite(evicts_file,  "\n");
        $fwrite(history_file, "\n");
        //$fwrite(delta_file,   "\n");
      end
      $fwrite(param_file,   "%d", initvar);
      //$fwrite(evicts_file,  "%d", initvar);
      $fwrite(history_file, "%d", initvar);
      //$fwrite(delta_file,   "%d", initvar);
    end
    //update line number
    if(s2_state==2'd0 && s3_state==2'd2) begin
      initvar = initvar+4096;
    end
    //update param_file 
    if(s2_state==2'd0 && s3_state==2'd2) begin
      total_attack = total_attack + attack_dectR;
      $fwrite(param_file, " evictsCR=12'b%b sac=%d/1qrm=%d,%d,%d max_evicts=%d/max_delta=%d/addr=%d max_history=%d/delta=%d/addr=%d attack=%d",
                            evictsCR, sac_valueR, qrm_decR, qrm_shdirR, qrm_shmntR, max_evictsR, max_deltaR, max_deladdrR, max_historyR, max_hisdeltaR, max_hisaddrR, attack_dectR);
      if(test_stop) begin
        $fwrite(total_file," ta=%d, mec=%d mae=%d msa=%d\n",
                total_attack + attack_dectR, max_evictsCR, max_all_evictsR, max_sac_valueR);
        $fwrite(32'h80000001," ta=%d, mec=%d mae=%d msa=%d\n",
                total_attack + attack_dectR, max_evictsCR, max_all_evictsR, max_sac_valueR);
      end
    end      
    //update evicts record file and compare it with input
    if(s2_state==2'd1) begin
       //$fwrite(evicts_file, " %d", s2_new_evicts);
       $fscanf(evicts_infile,  "%d", evicts_orig);     //read next set's evicts 
       if(evicts_orig>4095)begin                       //means have read the line_number 4096 8192....
         $fscanf(evicts_infile,  "%d", evicts_orig);   //read next set's evicts          
       end
       if(evicts_orig != s2_new_evicts) begin          //compare with input
         $fatal(0, "input error at %d %d\n", initvar,s2_addr);
       end
    end
    //update history file and delta file
    if(s2_state==2'd2) begin
       $fwrite(history_file, " %d",  {9{!(!deltapos & s2_new_history_temp[8])}} & s2_new_history_temp);
       //!deltapos & s2_new_history_temp[8] means underflow
       //$fwrite(delta_file,   " %d", delta_value);
    end
 end
end  

endmodule
