import numpy as np 
np.set_printoptions(threshold=np.inf)


f_emhistory = open("./output/emhistory.dat", 'w')
#f_swhistory = open("./input/403/403.gcc.200.history.log", 'r')
f_evicts    = open("./input/403/403.gcc.200.evict.log", 'r')

evicts = []

for line in f_evicts:
	evicts.append([float(x) for x in line.split()])

evicts = np.array(evicts)
row,cloum = evicts.shape
cloum=cloum-1

mu      = [float(0)]*row
sac     = [float(0)]*row            #square accumlate
qrm     = [float(0)]*row            #1/qrm
delta   = [[0 for i in range(cloum)] for i in range(row)]
history = [[0 for i in range(cloum)] for i in range(row)]

total_attack = 0

#print("%4d"%(row),"%4d"%(cloum))

#clear line number
for i in range(0, row): evicts[i,0]=0

#caculate mu
for i in range(0, row): 
	mu[i]=(sum(evicts[i]))/cloum 
 

#caculate accumlate
for i in range(0, row):                      
	sac_temp = 0
	for j in range(0, cloum):
		sac_temp=sac_temp+pow((evicts[i][j+1]-mu[i]),2)
	sac[i] = sac_temp
	

#caculate qrm
for i in range(0, row):
	if(sac[i]==0): qrm[i]=0                       
	else: qrm[i] = np.sqrt(1024/sac[i])
	#print("%8d"%(4096*(i+1)), qrm[i] ,file=f_emhistory)


#caculate delta
#row,cloum = delta.shape
for i in range(0, row):                      
	for j in range(0, cloum):
		evicts_temp=evicts[i][j+1]
		delta[i][j]=pow((evicts_temp-mu[i]),2)*qrm[i]
		if(evicts_temp<mu[i]): delta[i][j]=-delta[i][j]
	#print("%8d"%(4096*(i+1)), max(delta[i]) ,file=f_emhistory)


#caculate history
for i in range(0, row):
	dect=0
	dect_addr=0
	line_dis=4096*(i+1)
	print("%8d"%(line_dis), end=' ',file=f_emhistory)
	for j in range(0, cloum):
		if(i==0): history[i][j]=delta[i][j]
		else:     history[i][j]=history[i-1][j]*31/32+delta[i][j]
		if(dect):  history[i][j]=0
		if(history[i][j]>=160):
			dect=1 
			dect_addr=j
			total_attack=total_attack+1
		#elif(history[i][j]<0): history[i][j]=0
		print(" %.4f"%(history[i][j]), end=' ',file=f_emhistory)
	if(dect==1):
			for j in range(0, dect_addr+1):
				history[i][j]=0
	if((i+1)%1000 == 0): print(line_dis,"attack=%d"%(total_attack))
	print(file=f_emhistory)	

print("total_attack=%d"%(total_attack))	


f_emhistory.close()
#f_swhistory.close()
f_evicts.close()

