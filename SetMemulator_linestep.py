import numpy as np 
np.set_printoptions(threshold=np.inf)

f_emtotal   = open("./output/emtotal.dat", 'w+')
f_emhistory = open("./output/emhistory.dat", 'w+')
#f_swhistory = open("./input/403/403.gcc.200.history.log", 'r')
f_evicts    = open("./input/403/403.gcc.200.evict.log", 'r')

line=0
evicts  = []
history = [0]*1024
total_attack = 0

while(1):
	evicts=([float(x) for x in f_evicts.readline().split()])
	if not evicts: break
	evicts[0]=0
	evicts=np.array(evicts)
	(cloum,) = evicts.shape
	cloum = cloum-1
	#caculate mu
	mu=(sum(evicts))/cloum
	#caculate accumlate
	sac=0
	for i in range(0, cloum): sac=sac+pow((evicts[i+1]-mu),2)
	#caculate qrm
	qrm=0
	if(sac>0): qrm=np.sqrt(1024/sac)
	#caculate delta
	delta=[0]*cloum
	for i in range(0, cloum): 
		delta[i]=pow((evicts[i+1]-mu),2)*qrm
		if(evicts[i+1]<mu): delta[i]=-delta[i]
	#caculate history
	dect=0
	dect_addr=0
	line_dis=4096*(line+1)
	print("%8d"%(line_dis), end=' ',file=f_emhistory)                      
	for i in range(0, cloum):
		if(line==0): history[i]=delta[i]
		else:     history[i]=history[i]*31/32+delta[i]
		if(dect): history[i]=0
		if(history[i]>=160):    
			dect=1 
			dect_addr=i
			total_attack=total_attack+1
		#elif(history[i][j]<0): history[i][j]=0
		print(" %.4f"%(history[i]), end=' ',file=f_emhistory)
	if(dect==1):
			for i in range(0, dect_addr+1):
				history[i]=0
	line=line+1
	if(line%1000 == 0): print(line_dis,"attack=%d"%(total_attack))
	print(file=f_emhistory)		

print(4096*line, "total_attack=%d"%(total_attack))
print(total_attack,file=f_emtotal)

f_emtotal.close()	
f_emhistory.close()
#f_swhistory.close()
f_evicts.close()

	
	




