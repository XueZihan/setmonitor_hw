
module tb;

logic clk, rst;
wire access, evicted;
logic [9:0]  set_addr;
wire attack_detected;
wire [9:0] attack_addr;
wire busy;
wire test_stop;


SetCSAMonitor DUT(
  .clk(clk),
  .rst(rst),
  .access(access),
  .evicted(evicted),
  .set_addr(set_addr),
  .attack_detected(attack_detected),
  .busy(busy),
  .test_stop(test_stop)
);

/*
testmod DUT(
  .tclk(clk),
  .trst(rst),
  .taccess(access),
  .tevicted(evicted),
  .tset_addr(set_addr),
  .tattack_detected(attack_detected),
  .tbusy(busy)
);
*/

initial begin
  rst = 1;
  $fwrite(32'h80000001, "start\n");
  #100;
  rst = 0;
end

initial begin
  clk = 0;
  forever clk = #5 !clk;
end

/*
always @(posedge clk) begin
  if(rst) begin
    access   = 0;
    evicted  = 0;
    set_addr = 0;
  end else begin
    if(busy == 0) begin
      access   = 1;
      evicted  = 1;
      if(access==0) begin
        set_addr = 0;
      end else begin
        set_addr = set_addr+1;
      end
    end else begin
      access   = 0;
      evicted  = 0;
      set_addr = 0;
    end 
  end
end
*/

//debug input
reg endflag;
reg endflag_delay; //delay for $fwrite
reg busy_delay;
reg reachlastline;
longint unsigned lastline;

assign test_stop = endflag;

reg start;
reg evic_fin; //sent evict signal finished
reg[12:0] access_count;
reg[31:0] evicts_inpf;
reg[63:0] fdata;

initial begin
  evicts_inpf   = $fopen("./input/403/403.gcc.200.evict.log","r");
  //evicts_inpf   = $fopen("./input/403.gcc.cp-decl.evict.log","r");
end

assign evicted = start && !evic_fin && (fdata > 0 && fdata <= 31);
assign access  = evicted || (set_addr==10'd1023 && access_count<13'd4096);

always @(posedge clk) begin
  if(rst || busy) begin
    start        <= 1'b0;
    fdata        <= 1'b0;
    evic_fin     <= 1'b0;
    set_addr     <= 10'd0; 
    access_count <= 13'd0;
  end else begin
    if(1 < fdata && fdata <= 31) begin
      fdata <= fdata - 1'd1;     
    end else if(fdata > 31) begin
      start        <= 1'b1;
      set_addr     <= 10'd0;
      access_count <= 13'd0;
    end else if(start && set_addr<10'd1023) begin
      set_addr <= set_addr+1'd1;     
    end
    if(set_addr==10'd1023 && fdata<=1) begin
      evic_fin <= 1'b1;
    end
    if(access) begin
      access_count <= access_count+1'd1;
    end
    if(!endflag && (fdata>31 || (fdata <= 1 && set_addr<10'd1023))) begin
      $fscanf(evicts_inpf,"%d" ,fdata);     //read next set's evicts 
    end
  end
end

string    vcd_name = "";
longint   unsigned max_cycle = 0;
longint   unsigned cycle_cnt = 0;

initial begin
  $value$plusargs("max-cycles=%d", max_cycle);
end // initial begin

// vcd
initial begin
  if($test$plusargs("vcd"))
    vcd_name = "test.vcd";
    $value$plusargs("vcd_name=%s", vcd_name);
    if(vcd_name != "") begin
      $dumpfile(vcd_name);
      //$dumpvars(0, DUT);
      $dumpvars();
      $dumpon;
    end
end // initial begin

always @(posedge clk) begin
  if(rst) begin
    cycle_cnt     <= 0;
    endflag       <= 0;
    lastline      <= 64'd16805888;  //403 d16805888
    //lastline      <= 4861014016; //=>566046720 overflow???
    reachlastline <= 0;
  end else begin
    endflag    <= reachlastline && !busy && busy_delay;
    cycle_cnt  <= cycle_cnt + 1;
    busy_delay <= busy;
    endflag_delay <= endflag;
    //if(max_cycle == cycle_cnt)
    //  $fatal(0, "maximal cycle of %d is reached...", cycle_cnt);
    if(endflag_delay) begin
      $fatal(0, "all line(%d)'s analyse is finished...", lastline);      
    end
    if(fdata >= lastline) begin
      reachlastline <= 1'b1;
    end 
  end
 end

endmodule

