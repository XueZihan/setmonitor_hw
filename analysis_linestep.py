import numpy as np 
np.set_printoptions(threshold=np.inf)


f_analysis  = open("./output/analysis.dat", 'w+')
f_analtota  = open("./output/analtota.dat", 'w+')
#f_hwhistory = open("./output/emhistory.dat", 'r')
f_hwhistory = open("./output/hwhistory.dat", 'r')
f_emhistory = open("./output/emhistory.dat", 'r')
#f_swhistory = open("./input/403/t.log", 'r')
f_swhistory = open("./input/403/403.gcc.200.history.log", 'r')

hwhistory  = []
swhistory  = []
shhisdleta = [] #swhistory-hwhistory
line=0

'''
for line in f_hwhistory:
	hwhistory = ([float(x) for x in line.split()])
	hwhistory[0] = 0;
'''
hwdect =0 
swdect =0

while(1):
	swhistory=([float(x) for x in f_swhistory.readline().split()])
	hwhistory=([float(x) for x in f_hwhistory.readline().split()])
	if not swhistory: break
	if not hwhistory: break
	hwhistory[0]=0
	swhistory[0]=0
	hwhistory = np.array(hwhistory)
	swhistory = np.array(swhistory)
	(cloum,) = hwhistory.shape
	cloum = cloum-1
	shhisdleta = swhistory-hwhistory
	armetemp  = 0 
	rometemp  = 0 
	for i in range(0, cloum):
		armetemp  = armetemp+shhisdleta[i+1]
		rometemp  = rometemp+pow(shhisdleta[i+1],2)
	armedelta = armetemp/1024            #Arithmetic Mean
	romedelta = np.sqrt(rometemp/1024)   #Root Mean
	swmax_history = max(swhistory)
	hwmax_history = max(hwhistory)
	if(swmax_history>1):
		swmax_hisaddr = np.where(swhistory==swmax_history) 
		swmax_hisaddr = swmax_hisaddr[0][0]-1
	else:   swmax_hisaddr = 0
	if(hwmax_history>1):
		hwmax_hisaddr = np.where(hwhistory==hwmax_history) 
		hwmax_hisaddr = hwmax_hisaddr[0][0]-1
	else:   hwmax_hisaddr = 0
	sw_dect = bool(swmax_history>=160)
	hw_dect = bool(hwmax_history>=160)
	if(sw_dect): 
		swdect   = swdect+1
		sw_dectS = "SWDECT" 
	else:   sw_dectS = "SWNDEC"
	if(hw_dect): 
		hwdect   = hwdect+1
		hw_dectS = "HWDECT" 
	else:   hw_dectS = "HWNDEC"
	if(sw_dect != hw_dect):
		swhw_match="error"
	else:
		swhw_match="right"
	line_dis = 4096*(line+1)
	print("%8d"%(line_dis),"%-8.4f"%(swmax_history), "%4d"%(swmax_hisaddr), sw_dectS,\
                                 "%-8.4f"%(hwmax_history), "%4d"%(hwmax_hisaddr), hw_dectS,\
                                  swhw_match, "%-10.5f"%(armedelta), "%-10.5f"%(romedelta),\
                                                                                                  file=f_analysis)
	line = line+1
	if(line%1000 == 0): print(line_dis, "sw_dect=%d"%(swdect), "hw_dect=%d"%(hwdect))

print("sw_dect=%d"%(swdect), "hw_dect=%d"%(hwdect))
print("sw_dect=%d"%(swdect), "hw_dect=%d"%(hwdect), file=f_analtota)


f_hwhistory.close()
f_swhistory.close()
f_analysis.close()
f_analtota.close()
